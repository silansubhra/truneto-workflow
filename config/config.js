module.exports = {
	logging: true,
	seed: false,
	db: {
		url: 'mongodb://localhost/api'
	},
	googleAuth: {
		googleClientID: '13314824230-psttuk7b97l3b2bumdd1aecujsfj912l.apps.googleusercontent.com',
		googleClientSecret: 'NozUKaxyZrhoC4j4zr8T62TK'
	},
	smtp: {
		enableEmail: false,
		senderName: 'zinetgo',
		gmail: {
			host: 'smtp.gmail.com',
			port: 465,
			secure: true,
			auth: {
				user: 'support@zinetgo.com',
				pass: 'support@123'
			}
		}
    },
    neo4j: {
        "connection": {
            "protocol": "bolt",
            "host": "localhost",
            "port": 7687
        },
        "credentials": {
            "username": "neo4j",
            "password": "password"
        }
    },
    scheduledTasks: [
		{
			"name" : "PendingTaskProcessor",
			"schedule" : "* * * * * *"
        },
        {
			"name" : "completedTaskProcessor",
			"schedule" : "* * * * * *"
        },
        {
			"name" : "assignTask",
			"schedule" : "* * * * * *"
        }
	],
}