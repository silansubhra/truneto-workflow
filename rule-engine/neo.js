let neo4j = require('./index');
exports.getTaskWithRole = function (reqData, callback) {
    
    var query = "WITH " + genrateQueryStrObj(reqData) + " as params " +
    "MATCH(task:TR_WORKFLOW{name:params.name}) OPTIONAL MATCH (task)-->(role:TR_EMPLOYEE)" +
    " WITH task as task,role as role" +
    " RETURN {task:task,employee:role} AS endNode"
    
    neo4j.query(query,function(result, error){
        callback(error,parseMultipleFields(result))
    })
};

exports.getNextTask = function (queryObj, callback) {
   var queryString = "WITH "+ genrateQueryStrObj(queryObj) + " as params "+
   `MATCH (endNode:TR_WORKFLOW{name:params.name})-[rel:R_NEXT_TASK]->(startNode:TR_WORKFLOW) 
   WITH endNode, collect({nextTask:startNode,rel:rel}) as nextTask 
   RETURN { task:endNode, nextTask:nextTask} AS endNode`
   
   neo4j.query(queryString, function (result, err) {
       
        callback(err, parseResultNextTask(result));
    });
}



function parseResultNextTask(records) {
    var nextTaskArr = [];
    var nextTask = {};
    var tempArr;
    var nextTaskObj;
    var nextTaskList = [];
    records.forEach(function (record) {
        tempArr = record._fields;
        nextTaskObj = {};
        tempArr.forEach(function (task) {
            nextTaskObj.task = task.task.properties;
            task.nextTask.forEach(function(tempNext){
               // console.log(tempNext.nextTask,properties,'ooo');
                
                nextTaskArr = [];
                nextTask = {};
                if (tempNext.nextTask) {                    
                    tempNext.rel.properties.val = parseInt(tempNext.rel.properties.val);
                    nextTask = {
                        nextTask: tempNext.nextTask['properties'],
                        relation: tempNext.rel.properties
                    }
                    nextTaskList.push(nextTask);
                }
            })
        });
        nextTaskObj.nextTask = nextTaskList;
        nextTaskArr.push(nextTaskObj);
    })
    return nextTaskArr;
}


function genrateQueryStrObj(queryObj) {
	var queryStrObj = JSON.stringify(queryObj, replacer);
	queryStrObj.replace(/\\"/g,"\uFFFF"); //U+ FFFF
	queryStrObj = queryStrObj.replace(/\"([^"]+)\":/g,"$1:").replace(/\uFFFF/g,"\\\"");
	return queryStrObj;
}

function replacer(key, value) {
	if (typeof value === "string"){
		switch (value.toLowerCase ()) {
			case "true":
				return true;
			case "false":
				return false;
			default:
				if(isNaN(value)) {
					return value;
				} else {
					return parseInt(value);
				}
		}
	}
	return value;
}


function parseMultipleFields(records){    
    var taskWithRole = [];
    var taskRole = {};
    var tempArr ;
    records.forEach(function(record){
        tempArr  = record._fields;
        taskRole = {};
        tempArr.forEach(function(roleTask){
            taskRole.task = roleTask.task.properties;
            if (roleTask.role != null) {
                taskRole.role = roleTask.employee.properties;
            }else{
                taskRole.role = {}
            }
        });
        taskWithRole.push(taskRole);
    })
    return taskWithRole;
}