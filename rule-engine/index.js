const neo4j = require('neo4j-driver').v1;
const config = require('../config/config').neo4j;


var neo4jdriver = neo4j.driver(config.connection.protocol + "://" + config.connection.host + ":" + config.connection.port,
    neo4j.auth.basic(config.credentials.username, config.credentials.password));

exports.query = function (pQuery, callback) {
    var neo4jsession = neo4jdriver.session();
    neo4jsession.run(pQuery).then((result) => {
        if (result.records) {
            callback(result.records, null);
        } else {
            callback(null, { "msg": "Invalid Response" });
        }
        neo4jsession.close();
    }).catch((err) => {
        callback(null, err);
        neo4jsession.close();
    });
}