var express = require('express');
var router = express.Router();
var taskRule = require('../rule-engine/neo')
var Lead = require('../lead/leadModel')
var LeadHistory = require('../lead/leadHistoryModel')

/* GET users listing. */
exports.pendingTask = async function () {
  try{
    let pending_lead = await Lead.find({ portfolio_id: '1234567890', actionStatus: 'Pending' }) 
    console.log(pending_lead && pending_lead.length > 0);
    
    if (pending_lead && pending_lead.length > 0) {      
      for (let lead of pending_lead) {
        if (lead.TASK.type == 'manual' && lead.TASK.EPOCH > (Date.now() - 24 * 3600)) { //1 day buffer for task owner
          // mail to task owner and owner regarding the task
        } else if (lead.TASK.type == 'automatic') {
          //complete the work acccordingly  and then assign next task         
          taskRule.getNextTask({ portfolio_id: '1234567890', name: lead.TASK.name }, function (err, data) {
            if (!err) {              
              Lead.findOneAndUpdate({
                _id:lead._id
                },{$set: {TASK:data[0].nextTask[0].nextTask}}, {new:true}).then(function (err,result) {                  
                let leadHistroty = new LeadHistory(lead);
                leadHistroty.updated_at = new Date()
                //leadHistroty.save()
                // send mail or msg to task assingee
              })
            }
          })
        }
      }
    }
  }catch(err){
    console.log(err);
  }
};

exports.completedTask = async function () {
  let completed_lead = await LeadHistory.find({ portfolio_id: '1234567890', actionStatus: 'Completed' })  
  if (completed_lead & completed_lead.length > 0) {
    for (let lead of completed_lead) {
      if (lead.TASK.type == 'manual') { //1 day buffer for task owner
        taskRule.getNextTask({ portfolio_id: '1234567890', name: lead.TASK.name  }, function (err, data) {
          if (!err) {
            Lead.findOneAndUpdate({
              _id:lead._id
              },{$set: {TASK:data[0].nextTask[0].nextTask, actionStatus: 'Pending' }}, {new:true}).then(function (err,result) {                  
              let leadHistroty = new LeadHistory(lead);
              leadHistroty.updated_at = new Date()
              //leadHistroty.save()
              // send mail or msg to task assingee
            })
          }
        })
      }
    }
  }
};

exports.assignTask = async function(){  
  let unassigned_task = await LeadHistory.find({ portfolio_id: '1234567890', actionStatus: 'Pending', "OWNER" : { "$exists" : false } })  
  if (unassigned_task & unassigned_task.length > 0) {
    for (let lead of unassigned_task) {
      if (lead.TASK.type == 'manual') { //1 day buffer for task owner
        taskRule.getTaskWithRole({ portfolio_id: '1234567890', name: lead.TASK.name  }, function (err, data) {
          if (!err) {
            Lead.findOneAndUpdate({
              _id:lead._id
              },{$set: {OWNER:data[0].role}}, {new:true}).then(function (err,result) {                  
              let leadHistroty = new LeadHistory(lead);
              leadHistroty.updated_at = new Date()
            })
          }
        })
      }
    }
  }
}