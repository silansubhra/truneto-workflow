var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mongoose = require('mongoose');
const config = require('./config/config')
var schedule = require('node-schedule');
var taskprocessor = require('./routes/task')


var scheduledTasks = config.scheduledTasks;

mongoose.Promise = global.Promise;
mongoose.connect(config.db.url);

scheduledTasks.forEach(function(scheduledTask) {
	schedule.scheduleJob(scheduledTask.schedule, function() {
		executeScheduledTask(scheduledTask);
	})
});


function executeScheduledTask(task) {
  
	switch(task.name) {
		case 'PendingTaskProcessor' :
      taskprocessor.pendingTask();
			break;
    case 'completedTaskProcessor':
      taskprocessor.completedTask();
    break;
    case 'assignTask':
     taskprocessor.assignTask();
    break;
		default:
			console.log("Invalid Request Type");
	}
}

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


console.log('app is runnign successfully')

module.exports = app;
