//query to create table work flow

MERGE (:TR_WORKFLOW { name: 'NEW', type:'automatic',portfolio_id:'1234567890' })
MERGE (:TR_WORKFLOW { name: 'INSPECTION', type:'manual',portfolio_id:'1234567890' })
MERGE (:TR_WORKFLOW { name: 'QUOTATION', type:'automatic',portfolio_id:'1234567890' })

MATCH (startNode:TR_WORKFLOW {name:"NEW"}),(endNode:TR_WORKFLOW {name: 'INSPECTION'})
MERGE (startNode)-[:R_NEXT_TASK {val:0, var:"status"}]->(endNode)

MATCH (startNode:TR_WORKFLOW {name:"INSPECTION"}),(endNode:TR_WORKFLOW {name: 'QUOTATION'})
MERGE (startNode)-[:R_NEXT_TASK {val:0, var:"status"}]->(endNode)

//query to create table emploee
MERGE (:TR_EMPLOYEE { name: 'silan', email:'subhrajyoti34@gmail.com',portfolio_id:'1234567890' ,mobile:9066364784})

//query to assign relation between work flow and employee

MATCH (startNode:TR_WORKFLOW {name:"NEW"}),(endNode:TR_EMPLOYEE {mobile:9066364784})
MERGE (startNode)-[:R_TASK_ROLE]->(endNode)