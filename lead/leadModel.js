const mongoose = require( 'mongoose');
const {Schema} = require('mongoose');

var LeadSchema = new Schema({
    portfolioId: String,
    portfolio: {},
    branchId: String,
    orgId: String,
    customerId: String,
    customer: {},
    itemId: String,
    item: {},
    billNo: String,
    employeeId: String,
    employee: {},
    leadId: String,
    mpId: String,
    proLeadId: String,
    price: Number,
    rank: Number,
    slots: [{
        slotId: String,
        maxSlots: Number,
        slotFor: {
            type: String,
            enum: ["job","inspection"],
            default: "job"
        }
    }],
    aggregatorId: String,
    referenceNumber: String,
    referralId: Number,
    referralTrackingId: String, 
    couponId: String, 
    employementType: String,
    channel: String,
    city: String,
    dueDate: Date,
    leadSource: String,
    leadStatus: {
        type: String,
        default: "NEW"
    },
    address: String,
    geoLocation : String,
    referralType: {
        type: String, 
        enum: ["Direct","Vendor","Customer","Lead Source"], 
        default: "Direct"
    },    
    description: String,
    createdOn:{
        type: Date,
        default: Date.now 
    },
    createdBy: String,
    updatedBy: String,
    updatedOn: Date,
    status:{
        type: String,
        enum: ["active", "inactive"],
        required: true,
        default:'active'
    },
    TASK:{},
    OWNER:{},
    actionStatus:String,
    portfolio_id:String
});

module.exports = mongoose.model('lead', LeadSchema)